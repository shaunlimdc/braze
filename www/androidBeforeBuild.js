var fsCallback = function (err) {
    if (err) {
        console.error("Failed to create directory or file.");
        throw err;
    }
}
module.exports = function(context) {
    var fs = require('fs');
	var fsEx = require("fs-extra");

    console.log("Before build js for Android");

    // Copy CaptureFaceActivity.java
    fsEx.copy('plugins/cordova-plugin-appboy/src/android/google-services.json', 'platforms/android/app/google-services.json', { replace: true }, fsCallback);


    console.log('Customization done for Android');

};